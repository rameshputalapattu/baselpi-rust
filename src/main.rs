extern crate threadpool;
use threadpool::ThreadPool;
use std::sync::mpsc::{channel,Receiver,Sender};
use std::thread;
use std::time::Duration;







fn print_calculation_summary(rxpichan:&Receiver<f64>/*,term_count:i32*/) {


    const ANSI_CLEAR_SCREEN_SEQUENCE:&str = "\u{001B}[H\u{001B}[2J";
    const ANSI_FIRST_SLOT_SCREEN_SEQUENCE:&str = "\u{001B}[2;0H";
    const ANSI_SECOND_SLOT_SCREEN_SEQUENCE:&str = "\u{001B}[3;0H";
    println!("{}", ANSI_CLEAR_SCREEN_SEQUENCE);
    println!("{}Computed Value of Pi:\t\t{}", ANSI_FIRST_SLOT_SCREEN_SEQUENCE, rxpichan.recv().unwrap());
    //println!("{}# of Basel Terms:\t\t{}",ANSISecondSlotScreenSequence, term_count);



}


fn basel_term(i:f64,txpicalc:Sender<f64>)  {

    txpicalc.send(6.0/(i*i)).unwrap();

}

fn calculate_pi(num_terms:i32,txpichan:Sender<f64>,txcompdonechan:Sender<bool>) {

    let executors = ThreadPool::new(6);

    let (txpicalc,rxpicalc) = channel();

    for i in 1..num_terms+1 {
        let txpicalc = txpicalc.clone();
        executors.execute(move || {

            basel_term(i as f64,txpicalc.clone())

        })


    }

    let mut pival:f64 = 0.0;

    for i in 1..num_terms+1 {
        pival += rxpicalc.recv().unwrap();
        txpichan.send(pival.sqrt()).unwrap()



    }

   // txcompdonechan.send(true).unwrap();




}

fn main() {
    let (txpichan,rxpichan) = channel();

    let (txcompdonechan,rxcompdonechan) = channel();

    let print_thread = thread::spawn(move || loop {
        print_calculation_summary(&rxpichan);
        thread::sleep(Duration::from_millis(100))

    });

    let calculate_thread = thread::spawn(move || {
        calculate_pi(10000,txpichan,txcompdonechan);
    });

    //calculate_pi(10000,txpichan);
    print_thread.join().unwrap();
    calculate_thread.join().unwrap();






}
